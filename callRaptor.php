<?php 

$moduleName =$_GET['moduleName'];
$numberOfRecommendations =$_GET['numberOfRecommendations'];

$apiKey ="[Your API Key Here]"; //API key can be found in the controlpanel
$customerId = "[Your Customer Id Here]"; //customerid can be found in the controlpanel
$raptorUrl ="http://api.raptorsmartadvisor.com/v1/". $customerId ."/" . $moduleName ."/". $numberOfRecommendations ."/" . $apiKey;
$timeOutInSeconds= 1.5;

$ch = curl_init();
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept:application/json',));

curl_setopt($ch, CURLOPT_URL, $raptorUrl);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
curl_setopt($ch,CURLOPT_TIMEOUT,$timeOutInSeconds);
$response = curl_exec($ch);
curl_close($ch);


echo $response;
?>