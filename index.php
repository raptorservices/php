
<html>
<head>
        <script  src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
</head>
<body>

    <div id="output"></div>

    <script type="text/javascript">

        var moduleName ="GetOverAllTopVisits"; //See the Raptor Controlpanel for All available modules
        
        callRaptor(moduleName,10);

        function callRaptor( moduleName,numberOfRecommendations) {
            $.ajax({
                url:'callRaptor.php',
                data:{
                    moduleName: moduleName,
                    numberOfRecommendations: numberOfRecommendations
                },
                complete: function (response) {
                    var json = response.responseText;
                    $('#output').html(response.responseText);
                    //Render products
                    
                },
                error: function (error) {
                    $('#output').html(error);
                    //Log Error
                    //Return Fallback 
                }
            });
        }
    </script>

</body>